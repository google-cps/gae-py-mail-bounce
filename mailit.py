import webapp2

from google.appengine.api import app_identity
from google.appengine.api import mail

def send_mail():
    app_id = app_identity.get_application_id()
    sender = "anything@{}.appspotmail.com".format(app_id)
    to = "somesomesomesomesome@gmail.com"
    subject = "Testing bouncers"
    with open("unparseable.txt", "r") as content_file:
        body = content_file.read()

    mail.send_mail(sender=sender, to=to, subject=subject, body=body)

class MailSender(webapp2.RequestHandler):
    def get(self):
        send_mail()
        self.response.content_type = "text/plain"
        self.response.write("Sent unparseable email to {} wich should bounce".format(to))

app = webapp2.WSGIApplication([
    ("/send_mail", MailSender),
], debug=True)
