import logging
import re
import webapp2

from google.appengine.ext.webapp.mail_handlers import BounceNotificationHandler
from google.appengine.api import mail

BAD_WRAP_RE = re.compile('=\r\n')
BAD_EQ_RE = re.compile('=3D')

class BouncedEmail(BounceNotificationHandler):
    """Handler to notice when email to given user is bouncing."""
        
    # For docs on AppEngine's bounce email handling, see:
    # https://cloud.google.com/appengine/docs/python/mail/bounce
    # Source code is in file:
    # google_appengine/google/appengine/ext/webapp/mail_handlers.py
    
    def post(self):
        try:
            super(BouncedEmail, self).post()
        except AttributeError:
            # Work around for
            # https://code.google.com/p/googleappengine/issues/detail?id=13512
            raw_message = self.request.POST.get('raw-message')
            logging.info('raw_message %r', raw_message)
            raw_message = BAD_WRAP_RE.sub('', raw_message)
            raw_message = BAD_EQ_RE.sub('=', raw_message)
            logging.info('fixed raw_message %r', raw_message)
            mime_message = email.message_from_string(raw_message)
            logging.info('get_payload gives %r', mime_message.get_payload())
            self.request.POST['raw-message'] = mime_message
            super(BouncedEmail, self).post()  # Retry with mime_message

app = webapp2.WSDIApplication([
    ("/_ah/bounce", BouncedEmail),
], debug=True)
