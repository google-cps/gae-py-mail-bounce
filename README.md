# Bounce handler can't handle raw bounce message

When emails sent from an App Engine application bounce, they are sent back to the SMTP server.  This then sends a `POST` request to the sending application's `/_ah/bounce` handler with the original message stored in a `POST` variable named `raw_message`.  [Reported as of January 17, 2017][0], the content of `raw_message` in the `POST` request sent raises an error when passed to the BounceNotificationHandler contructor.

## Steps to reproduce with this source

* Get this reproduction and go to its directory

```shell
git clone git@bitbucket.org:cpstelus/gae-py-mail-bounce.git
cd ./gae-py-mail-bounce
```

* Deploy reproduction to production App Engine application

```shell
gcloud app deploy --version test --verbosity debug ./app.yaml
```

* Get application to send email to known invalid email

```shell
curl py-mail-bounce-dot-<your-project-id>.appspot.com/send_mail
```

* Inspect the logs with gcloud or Stackdriver Logging

```shell
gcloud app logs read --service py-mail-bounce --version test
```

[0]: https://code.google.com/p/googleappengine/issues/detail?id=13512